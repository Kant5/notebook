﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class NoteBook
    {
        private static Dictionary<int, Note> listNotes;

        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в \"Записную книжку\"!");
            listNotes = new Dictionary<int, Note>();
            ShowStartMenu();
            Console.ReadKey();
        }
        public static void ShowStartMenu()
        {
            int action=0;
            Console.WriteLine("Вы можете выполнить следующие действия в \"Записной книжке\":\n1 - Создать новую запись;\n2 - Редактировать созданные записи;\n3 - Просмотреть созданную запись;\n4 - Просмотреть все созданные записи;\n5 - Удалить созданные записи;\n6 - Выход");
            Console.Write("Введите номер действия, которое хотите выполнить: ");
            while(true)
            {
                try
                {
                    action = Convert.ToInt32(Console.ReadLine());
                    if (action > 0 && action < 7) break;
                    else Console.Write("Вы ввели не соответсвующий номер действия!! Попробуйте ввести еще раз: ");
                }
                catch (Exception ex)
                {
                    Console.Write("Вы ввели не соответсвующий номер действия!! Попробуйте ввести еще раз: ");
                    continue;
                }
            }
            switch (action)
            {
                case 1:
                    ShowMenuCreatNote();
                    break;
                case 2:
                    ShowMenuEditNote();
                    break;
                case 3:
                    ShowMenuReadNote();
                    break;
                case 4:
                    ShowMenuReadAllNotes();
                    break;
                case 5:
                    ShowMenuDeleteNote();
                    break;
                case 6:
                    Console.Clear();
                    Console.WriteLine("Очень жаль! До новых встречь.");
                    break;
            }
        }

        public static void ShowMenuCreatNote()
        {
            string action;
            Console.Clear();
            Console.WriteLine("Для создания записи необходимо заполнить поля. Обязательные поля будут помечены символом *.\nДля пропуска ввода нажимайте Enter.");
            Console.Write("Если хотите начать заполнять поля новой записи введите - \"да\",\nа для того чтобы вернуться в основное меню введите - \"нет\": ");
            while (true)
            {
                action = Console.ReadLine();
                if (action.Equals("нет", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    break;
                }
                else if (action.Equals("да",StringComparison.OrdinalIgnoreCase))
                {
                    CreatNewNote();
                    break;
                }
                else
                {
                    Console.Write("Попробуйте ввести еще раз: ");
                }
            }

        }
        public static void CreatNewNote()
        {
            string[] note=new string[8];
            string action;
            long number;
            DateTime date;
            Ecountry country;
            Console.WriteLine("Для того чтобы выйти в основное меню введите - \"выход\"");
            Console.WriteLine("Поля новой записи:");
            Console.Write("Фамилия*: ");
            while(true)
            {
                action = Console.ReadLine();
                if(action.Equals("выход",StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }else if(action.Equals(""))
                {
                    Console.Write("Это поле обязательное, поэтому необходимо его заполнить! Фамилия: ");
                    continue;
                }else
                {
                    note[0] = action;
                    break;
                }
            }
            Console.Write("Имя*: ");
            while (true)
            {
                action = Console.ReadLine();
                if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }
                else if (action.Equals(""))
                {
                    Console.Write("Это поле обязательное, поэтому необходимо его заполнить! Имя: ");
                    continue;
                }
                else
                {
                    note[1] = action;
                    break;
                }
            }
            Console.Write("Отчество: ");
            action = Console.ReadLine();
            if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
            {
                Console.Clear();
                ShowStartMenu();
                return;
            }
            else note[2] = action;
            Console.Write("Номер телефона* (вводить только цифры): ");
            while(true)
            {
                action = Console.ReadLine();
                if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }
                else
                {
                    try
                    {
                        number = Convert.ToInt64(action);
                        if (action.Length > 6 && action.Length < 12)
                        {
                            note[3] = action;
                            break;
                        }
                        else
                        {
                            Console.Write("Номер должен иметь больше цифр. Номер телефона: ");
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Это поле обязательное и необходимо ввести только цифры! Номер телефона: ");
                        continue;
                    }
                }
            }
            Console.Write("Выберите номер страны из списка и введите.\n1 - Россия;\n2 - США;\n3 - Испания;\n4 - Италия;\n5 - Франция\n");
            Console.Write("Страна*: ");
            while (true)
            {
                action = Console.ReadLine();
                if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }
                else
                {
                    try
                    {
                        number = Convert.ToInt32(action);
                        if (number > 0 && number < 6)
                        {
                            country = (Ecountry)Enum.Parse(typeof(Ecountry), (number - 1).ToString());
                            break;
                        }
                        else
                        {
                            Console.Write("Нет такого номера страны! Страна: ");
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Это поле обязательное и необходимо ввести только цифры! Страна: ");
                        continue;
                    }
                }
            }

            Console.Write("День рождения: ");
            while(true)
            {
                try
                {
                    action = Console.ReadLine();
                    if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.Clear();
                        ShowStartMenu();
                        return;
                    }
                    else if (!action.Equals(""))
                    {
                        date = DateTime.Parse(action);
                        note[4] = date.ToLongDateString();
                    }
                    break;
                }catch(Exception ex)
                {
                    Console.Write("Не правильный ввод даты. День рождения: ");
                    continue;
                }
            }
            Console.Write("Организация: ");
            action = Console.ReadLine();
            if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
            {
                Console.Clear();
                ShowStartMenu();
                return;
            }
            else note[5] = action;
            Console.Write("Должность: ");
            action = Console.ReadLine();
            if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
            {
                Console.Clear();
                ShowStartMenu();
                return;
            }
            else note[6] = action;
            Console.Write("Другие записи: ");
            action = Console.ReadLine();
            if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
            {
                Console.Clear();
                ShowStartMenu();
                return;
            }
            else note[7] = action;
            Note newNote = new Note(note,country);
            listNotes.Add(newNote.Id, newNote);
            Console.WriteLine("Запись успешно добавлена!");
            Console.Write("Для возвращения в основное меню нажмите на любую клавишу...");
            Console.ReadKey();
            Console.Clear();
            ShowStartMenu();
        }


        public static void ShowMenuEditNote()
        {
            Console.Clear();
            string action;
            int id;
            int number;//номер поля для редактирования
            if(listNotes.Count==0)
            {
                Console.WriteLine("В \"Записной книге\" нет ни одной записи, поэтому функция редактирования недоступна.");
                Console.Write("Если хотите создать новую запись введите - \"да\", а для возвращения в основное меню введите - \"нет\": ");
                while(true)
                {
                    action = Console.ReadLine();
                    if(action.Equals("да", StringComparison.OrdinalIgnoreCase)) ShowMenuCreatNote();
                    else if(action.Equals("нет", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.Clear();
                        ShowStartMenu();
                        return;
                    }else Console.Write("Попробуйте ввести еще раз: ");
                }
            }
            else
            {
                Console.Write("Для редактирования записи необходимо ввести его уникальный номер (ID): ");
                while(true)
                {
                    try
                    {
                        id = Convert.ToInt32(Console.ReadLine());
                        if (listNotes.ContainsKey(id))
                            break;
                        else Console.Write("С таким ID записи не существует! Попробуйте ввести другой номер: ");
                    }
                    catch(Exception ex)
                    {
                        Console.Write("Вы ввели не число! Попробуйте ввести еще раз: ");
                        continue;
                    }
                }
                EditNote(id);
            }
        }
        public static void EditNote(int id)
        {
            string action;
            long number;
            Console.WriteLine("Поля записи (обязательные поля отмечены символом *):\n1- Фамилия*;\n2 - Имя*;\n3 - Отчество;\n4 - Телефонный номер*;\n5 - Страна*;\n6 - Дата Дня Рождения;\n7 - Организация;\n8 - Должность;\n9 - Прочие записи;");
            Console.Write("Введите номер поля для его редактирования (для выхода в основное меню введите слово \"выход\": ");
            while (true)
            {
                action = Console.ReadLine();
                if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }
                else
                {
                    try
                    {
                        number = Convert.ToInt32(action);
                        if (number < 1 && number > 9) Console.Write("Такого номера поля нет. Попробуйте ввести еще раз: ");
                        else break;
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Не правильный ввод. Попробуйте еще раз: ");
                        continue;
                    }
                }
            }
            Console.Clear();
            Console.WriteLine("Для отмены ввода ничего не вводите и нажмите Enter.");
            Note note;
            listNotes.TryGetValue(id, out note);
            switch (number)
            {
                case 1:
                    Console.WriteLine("Поле Фамилия* содержит: " + note.Surname);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].Surname = action;
                        break;
                    }
                case 2:
                    Console.WriteLine("Поле Имя* содержит: " + note.Name);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записи...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].Name = action;
                        break;
                    }
                case 3:
                    Console.WriteLine("Поле Отчество содержит: " + note.MiddleName);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записи...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].MiddleName = action;
                        break;
                    }
                case 4:
                    Console.WriteLine("Поле Номер телефона* содержит: " + note.TelephoneNumber);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записи...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                            try
                            {
                                number = Convert.ToInt64(action);
                                if (action.Length > 6 && action.Length < 12)
                                {
                                    listNotes[id].TelephoneNumber = action;
                                    break;
                                }
                                else
                                {
                                    Console.Write("Номер не корректен и изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записи...");
                                    Console.ReadKey();
                                    EditNote(id);
                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.Write("Номер не корректен и изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записи...");
                                Console.ReadKey();
                                EditNote(id);
                                return;
                            }
                    }
                case 5:
                    Console.WriteLine("Поле Страна* содержит: " + note.Country);
                    Console.Write("Выберите номер страны из списка и введите.\n1 - Россия;\n2 - США;\n3 - Испания;\n4 - Италия;\n5 - Франция\n");
                    Console.Write("Для изменения поля введите номер страны: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        try
                        {
                            number = Convert.ToInt32(action);
                            if (number > 0 && number < 6)
                            {
                                listNotes[id].Country = (Ecountry)Enum.Parse(typeof(Ecountry), (number - 1).ToString());
                                break;
                            }
                            else
                            {
                                Console.Write("Нет такого номера страны и изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                                Console.ReadKey();
                                EditNote(id);
                                return;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Введены не цифры и изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                            Console.ReadKey();
                            EditNote(id);
                            return;
                        }
                    }
                case 6:
                    Console.Write("Поле Дата Дня Рождения содержит: ");
                    if (note.BirthDay != DateTime.MinValue) Console.WriteLine(note.BirthDay.ToLongDateString());
                    else Console.Write("\n");
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        try
                        {
                            DateTime date = DateTime.Parse(action);
                            listNotes[id].BirthDay = date;
                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Не правильный ввод даты и изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                            Console.ReadKey();
                            EditNote(id);
                            return;
                        }
                    }
                case 7:
                    Console.WriteLine("Поле Организация содержит: " + note.Organization);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].Organization = action;
                        break;
                    }
                case 8:
                    Console.WriteLine("Поле Должность содержит: " + note.Position);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].Position = action;
                        break;
                    }
                case 9:
                    Console.WriteLine("Поле Другие записи содержит: " + note.OtherNote);
                    Console.Write("Для изменения введите новое значение: ");
                    action = Console.ReadLine();
                    if (action.Equals(""))
                    {
                        Console.Write("Изменения не внесены!\nНажмите любую клавишу для выхода в меню редактирования записей...");
                        Console.ReadKey();
                        EditNote(id);
                        return;
                    }
                    else
                    {
                        listNotes[id].OtherNote = action;
                        break;
                    }
            }
            Console.Clear();
            Console.Write("Изменения сохранены.\nЕсли хоите продолжить редактирования записи введите - \"да\", а для возвращения в основное меню введите - \"нет\": ");
            while (true)
            {
                action = Console.ReadLine();
                if (action.Equals("да", StringComparison.OrdinalIgnoreCase))
                {
                    EditNote(id);
                    break;
                }
                else if (action.Equals("нет", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    ShowStartMenu();
                    break;
                }
                else Console.Write("Попробуйте ввести еще раз: ");
            }
        }




        public static void ShowMenuDeleteNote()
        {
            Console.Clear();
            string action;
            int id;
            if (listNotes.Count == 0)
            {
                Console.WriteLine("Функция удаления записи не доступна, так как не было создано ни одной записи.");
                Console.Write("Для выхода в основное меню нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
                ShowStartMenu();
            }
            else
            {
                Console.Write("Для удаления записи необходимо ввести уникальный номер записи (ID записи),\nа для выхода в основное меню введите слово \"выход\": ");
                while(true)
                {
                    action = Console.ReadLine();
                    if(action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.Clear();
                        ShowStartMenu();
                        return;
                    }else
                    {
                        try
                        {
                            id = Convert.ToInt32(action);
                            break;
                        }catch(Exception ex)
                        {
                            Console.Write("Введено не правильное значение. Попробуйте ввести еще раз: ");
                            continue;
                        }
                    }
                }
                if (listNotes.ContainsKey(id))
                {
                    DeleteNote(id);
                    Console.WriteLine("Запись успешно удалена!");
                    Console.Write("Для выхода в основное меню нажмите любую клавишу...");
                    Console.ReadKey();
                    Console.Clear();
                    ShowStartMenu();
                }
                else
                {
                    Console.Write("Записи с таким ID не существует.\nДля удаления другой записи введите - \"да\", а для выхода в основное меню введите - \"нет\": ");
                    while(true)
                    {
                        action = Console.ReadLine();
                        if (action.Equals("да", StringComparison.OrdinalIgnoreCase)) ShowMenuDeleteNote();
                        else if (action.Equals("нет", StringComparison.OrdinalIgnoreCase))
                        {
                            Console.Clear();
                            ShowStartMenu();
                        }
                        else Console.Write("Попробуйте ввести еще раз: ");
                    }
                }

            }
        }
        public static void DeleteNote(int id)
        {
            listNotes.Remove(id);
        }

        public static void ShowMenuReadNote()
        {
            Console.Clear();
            int id;
            string action;
            if(listNotes.Count==0)
            {
                Console.Write("Не было создано ни одной записи!\nДля выхода в основное меню нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
                ShowStartMenu();
            }
            else
            {
                Console.Write("Для просмотра записи введите ее уникальный номер (ID), а для выхода в основное меню введите - \"выход\": ");
                while (true)
                {
                    action = Console.ReadLine();
                    if (action.Equals("выход", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.Clear();
                        ShowStartMenu();
                        return;
                    }
                    else
                    {
                        try
                        {
                            id = Convert.ToInt32(action);
                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Введено не верное значение. Попробуйте ввести еще раз: ");
                            continue;
                        }
                    }

                }
                if (listNotes.ContainsKey(id))
                {
                    Console.WriteLine("Запись с ID " + id);
                    Console.Write(ReadNote(id));
                    Console.Write("Для выхода в основное меню нажмите любую клавишу...");
                    Console.ReadKey();
                    Console.Clear();
                    ShowStartMenu();
                    return;
                }
                else
                {
                    Console.Write("Записи с таким номером не создано.\nДля просмотра другой записи введите - \"да\", а для выхода в основное меню введите - \"нет\": ");
                    while (true)
                    {
                        action = Console.ReadLine();
                        if (action.Equals("да", StringComparison.OrdinalIgnoreCase)) ShowMenuReadNote();
                        else if (action.Equals("нет", StringComparison.OrdinalIgnoreCase))
                        {
                            Console.Clear();
                            ShowStartMenu();
                            break;
                        }
                        else
                        {
                            Console.Write("Попробуйте ввести еще раз: ");
                        }
                    }
                }
            }
            
        }
        public static string ReadNote(int id)
        {
            Note note;
            listNotes.TryGetValue(id, out note);
            string outString;
            outString = "Фамилия: " + note.Surname + "\n";
            outString += "Имя: " + note.Name + "\n";
            outString += "Отчество: " + note.MiddleName + "\n";
            outString += "Номер телефона: " + note.TelephoneNumber + "\n";
            outString += "Страна: " + ConvertCountry(note.Country.ToString()) + "\n";
            outString += "День рождения: ";
            if (note.BirthDay != DateTime.MinValue) outString += note.BirthDay.ToShortDateString() + "\n";
            else outString += "\n";
            outString += "Организация: " + note.Organization + "\n";
            outString += "Должность: " + note.Position + "\n";
            outString += "Другие заметки: " + note.OtherNote + "\n";
            return outString;
        }

        private static string ConvertCountry(string s)
        {
            switch(s)
            {
                case "Russia":
                    return "Россия";
                case "USA":
                    return "США";
                case "Spain":
                    return "Испания";
                case "Italia":
                    return "Италия";
                case "France":
                    return "Франция";
                default:
                    return "Такой страны нет";
            }
        }

        public static void ShowMenuReadAllNotes()
        {
            Console.Clear();
            if (listNotes.Count == 0)
            {
                Console.Write("Не было создано ни одной записи!\nДля выхода в основное меню нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
                ShowStartMenu();
            }else
            {
                ShowAllNotes();
                Console.Write("Для выхода в основное меню нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
                ShowStartMenu();
            }
        }
        public static void ShowAllNotes()
        {
            string outString;
            foreach (KeyValuePair<int, Note> it in listNotes)
            {
                Console.WriteLine("Запись с ID " + it.Key);
                outString = "Фамилия: " + it.Value.Surname + "\n";
                outString += "Имя: " + it.Value.Name + "\n";
                outString += "Номер телефона: " + it.Value.TelephoneNumber + "\n";
                Console.Write(outString);
                Console.WriteLine("----------------------------------------------------------");
            }
        }
    }
}
