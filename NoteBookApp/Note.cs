﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    public enum Ecountry : byte { Russia, USA, Spain, Italia, France };
    class Note
    {
        private static int countId = 0;


        public int Id { get; private set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string TelephoneNumber { get; set; }
        public Ecountry Country { get; set; }
        public DateTime BirthDay { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string OtherNote { get; set; }

        public Note(string[] note, Ecountry ecountry)
        {
            this.Id = ++countId;
            this.Surname = note[0];
            this.Name = note[1];
            this.MiddleName = note[2];
            this.TelephoneNumber = note[3];
            this.Country = ecountry;
            if (note[4] != null) this.BirthDay = DateTime.Parse(note[4]);
            this.Organization = note[5];
            this.Position = note[6];
            this.OtherNote = note[7];
        }
    }
}
